
## Suggested answers

### This attack will intercept unencrypted http traffic.

1. Yes. From: [https://wiki.wireshark.org/CaptureSetup/WLAN](https://wiki.wireshark.org/CaptureSetup/WLAN)

	*A 802.11 LAN uses a "broadcast medium", much like (the mostly obsolete shared) Ethernet. Compared to Ethernet, the 802.11 network is even "broader", as the transmitted packets are not limited by the cable medium.*

	Basicly all traffic i available to all hosts. You can sniff traffic by telling you wireless interface to read all traffic it sees, not only your own (commonly knows as promiscuous mode).

2. ettercap is flooding the network with arp traffic redirecting traffic between the VM host and WebGoat.
3. It will restore the ARP tables as they were before the attack aka "re arping".

### HTTP dissection attack with ettercap

1. Because ettercap is starts a "false" https endpoint serving up the false certificate. The commands sets up needed traffic redirection.

2. No. We need to redirect traffic to our own false endpoint no matter transmission medium.

###  SSL-Strip attack with arpspoof & sslstrip

1. By never using plain unencrypted HTTP. This can accomplished by plugins such as HTTPS Everywhere (chrome) and protocols like HSTS ([https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security))