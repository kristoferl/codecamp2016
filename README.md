# README #

Playground for the 2016 Code Camp.

### How do I get set up? ###

#### Dependencies ####
Install both of the following dependencies and make sure that `vagrant` is in your path by running `vagrant --version`.

- **Vagrant**
Download from [vagrantup.com](https://www.vagrantup.com/downloads.html) or install though brew/cask with `brew cask install vagrant`

- **VirtualBox**
Download from [virtualbox.com](https://www.virtualbox.org/wiki/Downloads) or install though brew/cask with `brew cask install virtualbox`  

We recommend making sure that the latest stable versions are installed.

#### Getting started ####
1. Clone this repository.
1. Make sure that the CWD is the repository you just cloned and run `vagarant up` from within it. (This is going to take while the first time you do this since the Vagrant images need to be downloaded). Some handy Vagrant info can be found in the [Vagrant](#markdown-header-vagrant) section.
1. When the command exits sucessfully you'll have two virtual machines running locally.
    * **Kali**

    	This is a ready to use installation of Kali-Linux 20016.1 ([https://www.kali.org](https://www.kali.org)). It' configuration is pristine except for customizations required by vagrant. This includes a running SSH server and a *vagrant* user. The keyboard has been changed to swedish for convenience.

    	To acccess: `vagrant ssh kali` (You can also use the gui: root/toor)

    * **WebGoat**

    	An ubuntu installation featuring a running instance of WebGoat ([link](https://www.owasp.org/index.php/Category:OWASP_WebGoat_Project)). The instance can be accessed directly on port **8080** or on port **80/443** through an proxying NGINX instance.  

    	The NGINX instance is configured with a certificate signed by a self signed CA. To get an enviroment as realistic as possible it is recommended to temporary add the CA to the certificate thrust and access the WebGoat instance by the signed certificates common name, `webgoat`. Of cource this is voluntary.  

    	The certificate files can be found at: `/root/certs`

    	To access: `vagrant ssh webgoat`


## Vagrant ##
Vagrant is a command line utility for managing the lifecycle of virtual machines. The vagrant definitions are located in a `Vagrantfile`. _Boxes_ in Vagrant are base images used for quickly cloning virtual machines.

The first time you run `vagrant up` in a directory containing a `Vagrantfile` the the boxes defined in the file will be downloaded (if they are not already on you system). All subsequent usage of the same boxes will use the already downloaded ones.

* **vagrant box list** - lists all you boxes on you system.
* **vagrant up** - starts and provisions the Vagrant environment.
* **vagrant status** - outputs status of the vagrant machine.
* **vagrant suspend** - suspends the machine.
* **vagrant halt** - stops the Vagrant machine.
* **vagrant resume** - resume a suspended Vagrant machine.
* **vagrant destory** - stops and deletes all traces of the vagrant machine.
* **vagrant help** - Vagrant help.
* **vagrant snapshot** - Manage snapshots.

## Network topology ##

Each vm instance is connected to two networks.

* **NAT network** (eth0)

	This is the netork that the instance uses to access the internet. The traffic will be NATed through the hosts network interface. The outside world will only be able to access ports that have been explicitly port forwarded to the instance.

* **Host-only network** (eth1)

  This is a network that all instances + the host machine is connected to. It is basicly a private LAN where all connected nodes can communicate without any firewalls etc.